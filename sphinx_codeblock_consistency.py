# © 2024 ETH Zurich, Scientific IT Services

"""Sphinx extension checking consistency of code blocks with source files.

The extension will search for code blocks (`.. sourcecode::`) in rst files.
For each code block with a class (`:class:` option) containing
`src=<src path>:<line number>`, it will check if the file at `<src path>`
contains the content of the code block at line number `<line number>`.
On mismatches, it aborts sphinx and reports diffs.
"""

import os
import itertools
from typing import Optional, Any
from collections.abc import Iterable
import difflib

from sphinx.errors import ExtensionError
from sphinx.application import Sphinx

__version__ = "0.0.1"


def setup(app: Sphinx) -> None:
    """Add extension to sphinx."""
    app.add_config_value("sphinx_codeblock_consistency_src_path", ".", rebuild="env")
    app.connect("source-read", check)
    app.connect("doctree-resolved", report_errors)


def check(app: Sphinx, docname: str, source: list[str]) -> None:
    """Scan for inconsistencies."""
    env = app.builder.env
    if not hasattr(env, "sphinx_codeblock_consistency_errors"):
        env.sphinx_codeblock_consistency_errors = []  # type: ignore[attr-defined]
    errors = env.sphinx_codeblock_consistency_errors  # type: ignore[attr-defined]
    src_path = app.config.sphinx_codeblock_consistency_src_path
    for path, line_no, code in find_code_blocks(source[0]):
        abs_path = os.path.realpath(os.path.join(src_path, path))
        try:
            delta = diff(abs_path, line_no, code, docname)
            if delta is not None:
                errors.append("".join(delta))
        except FileNotFoundError:
            errors.append(f"{docname}: Source file does not exist: {abs_path}")


def report_errors(app: Sphinx, _doctree: Any, _fromdocname: str) -> None:
    """Abort sphinx and report errors."""
    errors = getattr(app.builder.env, "sphinx_codeblock_consistency_errors", [])
    if errors:
        raise ExtensionError(
            "💥 Code block inconsistencies detected:\n" + "\n".join(errors),
            modname=__name__,
        )


def diff(
    path: str, line_no: int, code: list[str], doc_path: str
) -> Optional[Iterable[str]]:
    """Compare the content of `path` at line number `line_no` to `code`.

    On a mismatch return a diff using `doc_path` as a caption for the
    version given in `code`.
    """
    with open(path, encoding="utf-8") as stream:
        content = stream.read().splitlines()
    reference_code = content[line_no - 1 : line_no - 1 + len(code)]
    if reference_code != code:
        return difflib.context_diff(
            [line + "\n" for line in code],
            [line + "\n" for line in reference_code],
            fromfile=doc_path,
            tofile=path,
        )
    return None


def find_code_blocks(body: str) -> Iterable[tuple[str, int, list[str]]]:
    """Extract code blocks from `body`.

    Returns an iterable of tuples consisting
    of the path and line number (specified by the class argument of the code
    block) of the reference code and the code contained in the code block.
    """
    lines = body.splitlines()
    code_block_lines = [
        n for n, line in enumerate(lines) if line.startswith(".. sourcecode::")
    ]
    code_blocks = [read_code_block(lines[start:]) for start in code_block_lines]
    for code, arguments in code_blocks:
        classes = arguments.get("class", "").split()
        try:
            src = next(
                part for part in classes if part.startswith("src=")
            ).removeprefix("src=")
        except StopIteration:
            continue
        try:
            path, line_no = src.split(":", 1)
            n = int(line_no)
            yield path, n, code
        except ValueError:
            raise RuntimeError(f"Invalid source file specified: {src}") from None


def read_code_block(lines: list[str]) -> tuple[list[str], dict[str, str]]:
    """Parse a rst code block."""
    code_block_lines = [
        line.removeprefix("   ")
        for line in list(
            itertools.takewhile(
                lambda line: not line or line.startswith("   "), lines[1:]
            )
        )
    ]
    while code_block_lines and not code_block_lines[-1]:
        code_block_lines.pop()
    argument_lines = list(itertools.takewhile(bool, code_block_lines))
    code_lines = code_block_lines[len(argument_lines) + 1 :]
    arguments = dict(
        arg_line.removeprefix(":").split(": ", 1) for arg_line in argument_lines
    )
    return code_lines, arguments
