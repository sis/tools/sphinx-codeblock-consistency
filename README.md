# sphinx-codeblock-consistency

Sphinx extension checking consistency of code blocks with source
files.

The extension will search for code blocks (`.. sourcecode::`) in rst
files.
For each code block with a class (`:class:` option) containing
`src=<src path>:<line number>`, it will check if the file at
`<src path>` contains the content of the code block at line number
`<line number>`.
On mismatches, it aborts sphinx and reports diffs.

## Install

```sh
git+https://gitlab.ethz.ch/sis/tools/sphinx-codeblock-consistency.git
```

## Usage

Add 

```py
extensions = ["sphinx_codeblock_consistency"]
sphinx_codeblock_consistency_src_path = "<path to your source code root>"
)
```

to your `conf.py`
